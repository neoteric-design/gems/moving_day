# MovingDay Changelog

## 0.2.0

* Add support for outputting YAML flavored front matter
* Add `MovingDay.attributes_to_front_matter` helper method/public API to avoid needing referencing internals


## 0.1.0

* Initial release
