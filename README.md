# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# MovingDay

Export Neoteric CMS projects to markdown. The goal is to provide a simple way to
generate a markdown document from a given record. Generally through mapping
attributes to front matter and compiling all the body content into an HTML
document and converting to markdown.

## Installation
Add this line to your application's Gemfile:

```ruby
gem "moving_day"
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install moving_day
```

Then install with 

```bash
$ bin/rails g moving_day:install
```

If you have custom Neoteric Block types, you can extend the BlockExtruder with
methods for compiling its html in the initializer provided

## Usage

Generate an exporter for a model

```bash
$ bin/rails g moving_day:exporter Event
```

This generates an exporter class that can be customized. 
See [the base exporter](lib/moving_day/exporter.rb) for the API provided and extend as needed.

It also generates an export task in your application's `lib/tasks/moving_day.rake` that can also be
customized. After reviewing and adapting the generated code, you can export a
model with: 

```bash
# Using example above
$ bin/rails moving_day:events
```


## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
