require_relative 'lib/moving_day/version'

Gem::Specification.new do |spec|
  spec.name        = 'moving_day'
  spec.version     = MovingDay::VERSION
  spec.authors     = ['Madeline Cowie']
  spec.email       = ['madeline@cowie.me']
  spec.homepage    = 'https://www.neotericdesign.com'
  spec.summary     = 'Tools to help migrate from Neoteric CMS to Markdown static sites'
  spec.description = 'Tools to help migrate from Neoteric CMS to Markdown static sites'
  spec.license     = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.

  spec.metadata['homepage_uri'] = spec.homepage
  #  spec.metadata["source_code_uri"] = "https://gitlab.com"
  #  spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  spec.add_dependency 'nokogiri'
  spec.add_dependency 'rails', '>= 5'
  spec.add_dependency 'reverse_markdown'
end
