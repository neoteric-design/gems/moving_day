require 'reverse_markdown'
require 'nokogiri'
require 'moving_day/version'
require 'moving_day/railtie'
require 'moving_day/exporter'
require 'moving_day/front_matter'
require 'moving_day/block_extruder'

module MovingDay
  def self.html_to_md(html)
    ReverseMarkdown.convert(extract_and_append_footnotes(html))
  end

  def self.attributes_to_front_matter(format = :toml, **attributes)
    case format
    when :toml
      FrontMatter.to_toml(attributes)
    when :yaml
      FrontMatter.to_yaml(attributes)
    else
      raise ArgumentError, 'Unknown Format'
    end
  end

  def self.extract_and_append_footnotes(html)
    doc = Nokogiri::HTML(html)
    idx = 1

    doc.css('.inline-note').each do |node|
      doc.at_css('body').add_child("<p>[^#{idx}]: #{node.inner_html}</p>")
      node.content = "[^#{idx}]"
      idx += 1
    end

    doc.to_html
  end

  def self.write_file(basedir:, filename:, content:)
    FileUtils.mkdir_p(basedir)

    full_path = "#{basedir}#{filename}"

    puts "Writing: #{full_path}"
    File.open(full_path, 'w+') do |f|
      f << content
    end
  end

  def self.download_attachment(attachment, local_path)
    FileUtils.mkdir_p(Pathname.new(local_path).parent)
    attachment.copy_to_local_file(:original, local_path)
  end
end
