module MovingDay
  class Exporter
    attr_reader :record

    delegate_missing_to :record

    def initialize(record)
      @record = record
    end

    # Filename to write to
    def output_filename
      return "#{slug}.md" if respond_to?(:slug)

      "#{self.class.name}--#{id}.md"
    end

    # Hash of attributes used to build front matter content.
    def front_matter_attributes
      {
        title: title
      }
    end

    # Final string of front matter contents except +++ dividers for resource
    def front_matter
      MovingDay.attributes_to_front_matter(front_matter_format, front_matter_attributes)
    end

    def front_matter_format
      :toml
    end

    # Full body content of resource as html, for conversion
    def html_body
      if respond_to?(:blocks)
        MovingDay::BlockExtruder.to_html(blocks)
      elsif respond_to?(:body)
        body
      else
        raise 'Unable to find blocks or a body field. Please define method html_body in your exporter with desired content'
      end
    end

    # Final string of markdown body contents for resource
    def markdown_body
      MovingDay.html_to_md html_body
    end

    # Final string of complete markdown contents
    def to_markdown
      [front_matter, "\n\n", markdown_body].join
    end

    # Full output path to write to. Must end in slash
    def output_folder
      "./export/content/#{content_subfolder}/"
    end

    # Subfolder name used in output_folder
    def content_subfolder
      record.class.to_s.underscore
    end

    def export_md(basedir: output_folder, filename: output_filename, content: to_markdown)
      MovingDay.write_file(basedir: basedir, filename: filename, content: content)
    end
  end
end
