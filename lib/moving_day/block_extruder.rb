module MovingDay
  class BlockExtruder
    attr_reader :block

    def self.to_html(blocks)
      blocks.map { |b| new(b).to_html }.reject(&:blank?).join
    end

    def initialize(block)
      @block = block
    end

    def to_html
      extrude
    end

    def extrude
      public_send "extrude_#{block.class.to_s.demodulize.underscore}"
    end

    def extrude_rich
      block.body
    end
  end
end
