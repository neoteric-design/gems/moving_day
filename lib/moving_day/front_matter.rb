# frozen_string_literal: true

module MovingDay
  class FrontMatter
    def self.to_toml(**attributes)
      new(attributes).to_toml
    end

    def self.to_yaml(**attributes)
      new(attributes).to_yaml
    end

    def initialize(**attributes)
      @attributes = attributes
    end

    def to_toml
      @attributes.map do |key, value|
        toml_attribute_line(key, value)
      end.prepend('+++').append('+++').join("\n")
    end

    def to_yaml
      @attributes.map do |key, value|
        yaml_attribute_line(key, value)
      end.prepend('---').append('---').join("\n")
    end

    private

    def toml_attribute_line(key, value)
      if front_matter_raw_datatypes.include?(value.class)
        %(#{key} = #{value})
      else
        %(#{key} = "#{value}")
      end
    end

    def yaml_attribute_line(key, value)
      if value.is_a?(Array)
        "#{key}:\n" + value.map { |val| %(\t- #{val}) }.join("\n")
      elsif front_matter_raw_datatypes.include?(value.class)
        %(#{key}: #{value})
      else
        %(#{key}: "#{value}")
      end
    end

    def front_matter_raw_datatypes
      [Date, Time, ActiveSupport::TimeWithZone, TrueClass, FalseClass, Array]
    end
  end
end
