class MovingDay::ExporterGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)

  def add_exporter
    template "exporter.erb", "app/exporters/#{singular_table_name}_exporter.rb"
  end

  def add_task
    insert_into_file "lib/tasks/moving_day.rake",
      after: "namespace :moving_day do\n" do
        <<~RAKE

            desc "Export #{plural_name} to markdown"
            task #{plural_table_name}: :environment do
              #{name}.find_each do |#{singular_table_name}|
                #{name}Exporter.new(#{singular_table_name}).export_md
              end
            end
        RAKE
      end
  end
end
