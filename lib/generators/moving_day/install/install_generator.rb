class MovingDay::InstallGenerator < Rails::Generators::Base
  source_root File.expand_path('templates', __dir__)

  def add_initializer
    initializer "moving_day.rb" do
      <<~CODE
        MovingDay::BlockExtruder.class_eval do
          # Extend BlockExtruder here for custom blocks
          # e.g for Blocks::ReadMore :
          #
          # def extrude_read_more
          #   "<hr>"
          # end
        end
      CODE
    end
  end

  def add_rake_base
    rakefile "moving_day.rake" do
      <<~RAKE
        namespace :moving_day do
        end
      RAKE
    end
  end
end
